//
//  GenreViewController.swift
//  Movie_Omel
//
//  Created by admin on 18.01.22.
//

import UIKit

class GenreViewController: UIViewController {
        
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.dataSource = self
    }
   
}

extension GenreViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 100
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GengeCollectionViewCell.identifier, for: indexPath) as! GengeCollectionViewCell
        
        
        return cell
    }
}
